import os
import time
import redis
import random

port = os.getenv("REDIS_PORT", 6379)
red = redis.StrictRedis('localhost', port=port, charset="utf-8", decode_responses=True)

def produce():
  # items published in a list persist unless removed
  # it can be used as a message queue
  value = random.randrange(0,100000)
  # push to the top
  red.lpush('queue', value)
  print(value)

if __name__ == "__main__":
  while True:
    produce()
    time.sleep(random.randrange(0,3))
