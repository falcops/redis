import os
import redis
import time
import random

port = os.getenv("REDIS_PORT", 6379)
red = redis.StrictRedis('localhost', port=port, charset="utf-8", decode_responses=True)

def publish():
  # items published are lost if not consumed by a subscribed consumer actively listening
  # more subscribers can consume items at the same time
  # TODO: publish json
  value = random.randrange(0,100000)
  red.publish('numbers', value)
  print(value)

if __name__ == "__main__":
  while True:
    publish()
    time.sleep(random.randrange(0,3))
