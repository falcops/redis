import os
import redis

port = os.getenv("REDIS_PORT", 6379)
red = redis.StrictRedis('localhost', port=port, charset="utf-8", decode_responses=True)

def consume():
  sub = red.pubsub()
  sub.subscribe('numbers')
  for counter, message in enumerate(sub.listen()):
    if message is not None and isinstance(message, dict):
      # TODO: consume json
      value = message.get('data')
      print(counter, value)

if __name__ == "__main__":
  while True:
    consume()
