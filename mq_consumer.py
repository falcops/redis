# reliable consumption of items from a list
# items can be only consumed by one party at a time

import os
import redis

def consume():
  """
  second list is used to implement reliable consumption
  as it avoids multiple consumers working on the same message
  and it allows a mechanism to recover items put in "processing" queue in case of error
  """
  red.blmove(first_list="queue", second_list="processing",src="RIGHT", dest="LEFT", timeout=0)
  # 1. get value of item added to top of second_list
  value = red.lindex(name="processing", index=0)
  # 2. do something, here we just print
  print("queue items:", red.llen("queue"), "processing items:", red.llen("processing"), f"#{counter}", value)
  # 3. drop from second_list
  red.lrem(name="processing", value=value, count=0)

if __name__ == "__main__":
  port = os.getenv("REDIS_PORT", 6379)
  red = redis.StrictRedis('localhost', port=port, charset="utf-8", decode_responses=True)
  counter = 0
  while True:
    consume()
    counter += 1
