# redis

## Getting started

1. run redis `make redis`
2. create venv `make env`
3. activate it `source venv/bin/activate`
4. install deps `pip install -r requirements.txt`

## patterns

### 1. pub/sub

* publisher adds message to topic
* topic subscriber(s) receive it
* messages are ephemeral, they are not stored anywhere for later consumption. Subscribers must be up and running or will lose new messages.
* multiple subscribers/publishers for each topic
* suitable for data stream handling, e.g. logging
* https://redis.io/docs/manual/pubsub/
* blocking by default, easy to use

### 2. reliable message queue

* can be implemented in a variety of ways (e.g. redis Lists, Sets...)
* producer adds items to a data structure such a List
* consumer removes item from data structure to perform operation
* items are persisted in the data structure until consumption
* items can be consumed only by one consumer (however one can adopt creative ways to work around this)
* issues:
  - concurrency: multiple consumers may try to consume same item at once
  - error handling: what if consumer fail after pulling item from list but before processing it?
* solution: reliable message queue pattern
  - [LMOVE](https://redis.io/commands/lmove/)
  - move item from first list (public=watched by multiple consumers) to second list (private=watched by single consumer)
